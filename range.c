#include<unistd.h>
#include<stdio.h>
#include<math.h>
FILE *fop;
int range_func(int[],int);
int iqr_func(int[],int,int);
float sd_func(int[],int);
float cv_func(int[],int);
void main(char argc, char **argv)
{
	FILE *fp;
	int i=0,j=0,a[100],n=0,temp,c,total,range,iqr;
	float sd,cv;
	fp = fopen(argv[1],"r");
	fop = fopen("output.c","a+");
	while(fscanf(fp,"%d",&a[i]) > 0)
	{
	 printf("%d\n",a[i]);
	 i++;
	n++;
	}
	printf("count is %d\n",n);
	printf("Sorted array \n");
	for(i=0;i<n;i++)
	{
		for(j=i+1;j<n;j++)
		{
			if(a[i]>a[j])
			{
				temp=a[i];
				a[i]=a[j];
				a[j]=temp;
			}
		}
	}
	printf("\n Enter your choice:\n 1.Range \n 2. Interquartile range \n 3.Standard Deviation \n 4.Coefficient of Variation\n");
	scanf("%d",&c);
	switch(c)
	{
		case 1 : range=range_func(a,n);
			 fprintf(fop,"\nRange = %d",range);
			 break;
		case 2 : iqr=iqr_func(a,n,total);
			 fprintf(fop,"\nInter quartile range = %d",iqr);
			 break;
		case 3 : sd=sd_func(a,n);
			 fprintf(fop,"\nStandard Deviation = %f",sd);
			 break;
		case 4 : cv=cv_func(a,n);
			 fprintf(fop,"\ncoefficient of variation = %f",cv);
			 break;
		default :printf("\nWrong Choice");
	}
}
int range_func(int a[],int n){
	int range=a[n-1]-a[0];
			
	return range;
}
int iqr_func(int a[], int n, int total)
{
	if(n%2==0){
		total = n;
	}
	else{
		total = n+1;
	}
	int lower = (.25)*total;
	printf("\n Total: %d\t",lower);
	int upper = (.75)*total;
	float lower_value = (a[lower]+a[lower+1])/2;
	float upper_value = (a[upper]+a[upper+1])/2;
	float total_value = upper_value-lower_value;
	
return total_value;
}

float sd_func(int a[], int n){
	float avg,ch,var;
	float sd,b[50];
	int i;
	for(i=0;i<n;i++)
				avg=avg+a[i];
			 avg=avg/n;
			 for(i=0;i<n;i++)
			 {
				ch=a[i]-avg;
				ch=ch*ch;
				b[i]=ch;
			 }
			 for(i=0;i<n;i++)
				var=var+b[i];
			 var=var/n;
			 sd=sqrt(var);
			return sd;
			 
}
float cv_func(int a[], int n)
{
	int i;
	float avg;
	float sd,cv;
	for(i=0;i<n;i++)
		avg=avg+a[i];
	avg=avg/n;
	sd= sd_func(a,n);
	cv=sd/avg;
	
	return cv;
}